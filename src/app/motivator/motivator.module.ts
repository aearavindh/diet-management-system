import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MotivatorComponent } from './motivator.component';



@NgModule({
  declarations: [MotivatorComponent],
  imports: [
    CommonModule
  ]
})
export class MotivatorModule { }
