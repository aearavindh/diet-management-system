import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdministratorComponent } from './administrator.component';



@NgModule({
  declarations: [AdministratorComponent],
  imports: [
    CommonModule
  ]
})
export class AdministratorModule { }
