import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChallengerComponent } from './challenger.component';



@NgModule({
  declarations: [ChallengerComponent],
  imports: [
    CommonModule
  ]
})
export class ChallengerModule { }
